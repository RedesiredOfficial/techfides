/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */


const $ = require('jquery');
window.$ = $;
window.jQuery = $;
require('popper.js');
require('jqueryui');
require('bootstrap-datepicker');
require('bootstrap');
const dt = require( 'datatables.net' )();
const buttons = require( 'datatables.net-buttons' )();
require('datatables.net-dt');
// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');


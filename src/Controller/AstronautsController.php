<?php
/**
 * Created by PhpStorm.
 * User: Radovan Bartánus
 * Date: 6.8.2018
 * Time: 1:38
 */

namespace App\Controller;
use App\Entity\Astronaut;
use App\Form\AstronautType;
use App\Repository\AstronautRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AstronautsController
 * @package App\Controller
 * Controller handling the display and the management of astronauts
 */
class AstronautsController extends AbstractController
{
	/**
	 * @Route("/", methods={"GET"},  name="list")
	 */
	public function astronautList()
	{
		$addForm = $this->createForm(AstronautType::class, NULL,
			['action' => $this->generateUrl('add_astronaut'),
			'method' => 'POST',
			'attr' => ['id' => 'addForm']]);
		$editForm = $this->createForm(AstronautType::class, NULL,
			['action' => $this->generateUrl('edit_astronaut', ['id' => 0]),
			'method' => 'PUT',
			'attr' => ['id' => 'editForm']]);
		/** @var AstronautRepository $astroRepository */
		$astroRepository = $this->getDoctrine()->getRepository(Astronaut::class);
		$astronauts = $astroRepository->findAll();
		return $this->render('\astronauts\index.html.twig', ['addForm' => $addForm->createView(), 'editForm' => $editForm->createView(), 'astronauts' => $astronauts]);
	}

	/**
	 * @Route("/add", methods={"POST"}, name="add_astronaut")
	 */
	public function addAstronaut(Request $request)
	{
		$astronaut = new Astronaut();
		$form = $this->createForm(AstronautType::class, $astronaut,
			['action' => $this->generateUrl('add_astronaut'),
			'method' => 'POST',
			'attr' => ['id' => 'addForm']]);
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()) {
			/** @var EntityManager $em */
			$em = $this->getDoctrine()->getManager();
			$em->persist($astronaut);
			$em->flush();
			return $this->json(['success' => true, 'newAstronaut' => $astronaut]);
		}
		return $this->json(['success' => false]);
	}


	/**
	 * @Route("/{id}" , methods={"DELETE"}, name="delete_astronaut")
	 */
	public function deleteAstronaut(Astronaut $astronaut)
	{
		if($astronaut) {
			/** @var EntityManager $em */
			$astronautId = $astronaut->getId();
			$em = $this->getDoctrine()->getManager();
			$em->remove($astronaut);
			$em->flush();
			return $this->json(['success' => true, 'id' => $astronautId]);
		}
		return new Response('', 404 );
	}

	/**
	 * @Route("/{id}", requirements={"id"="\d+"}, methods={"PUT"}, name="edit_astronaut")
	 */
	public function editAstronaut(Request $request, Astronaut $astronaut)
	{
		if($astronaut) {
			$form = $this->createForm(AstronautType::class, $astronaut,
				['action' => $this->generateUrl('edit_astronaut', ['id' => $astronaut->getId()]),
				'method' => 'PUT',
				'attr' => ['id' => 'editForm']]);
			$form->handleRequest($request);
			if($form->isSubmitted() && $form->isValid()) {
				/** @var EntityManager $em */
				$em = $this->getDoctrine()->getManager();
				$em->persist($astronaut);
				$em->flush();
				return $this->json(['success' => true, 'editedAstronaut' => $astronaut]);
			}
			return $this->json(['success' => false]);
		}
		return new Response('', 404 );
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: Radovan Bartánus
 * Date: 6.8.2018
 * Time: 1:23
 */

namespace App\Form;


use App\Entity\Astronaut;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AstronautType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('name', TextType::class)
			->add('surname', TextType::class)
			->add('birthdate', DateType::class,
				['widget' => 'single_text',
				'html5' => false,
				'attr' => ['class' => 'js-datepicker'],
					'format' => 'dd.MM.yyyy'])
			->add('superpower', TextType::class, ['required' => false])
			->setAction($options['action'])
			->setMethod($options['method'])
			->setAttribute('id', $options['attr']['id'])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'attr' => ['id' => 'addForm'],
			'data_class' => Astronaut::class,
		));
	}
}